let result = [
    {
        "id": 1,
        "name": "tommy",
        "breed": "labrador retriever",
        "colour": "chocolate",
        "age": 5.4,
        "next_checkup": "2019-12-27",
        "vaccinations": ["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
        "sold" : false
    },
    {
        "id": 2,
        "name": "jia",
        "breed": "english mastiff",
        "colour": "yellow",
        "age": 6,
        "next_checkup": "2020-1-2",
        "vaccinations": ["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
        "sold" : false
    },
    {
        "id": 3,
        "name": "xena",
        "breed": "golden retriever",
        "colour": "yellow",
        "age": 4.5,
        "next_checkup": "2020-2-6",
        "vaccinations": ["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
        "sold" : false
    },
    {
        "id": 4,
        "name": "bruno",
        "breed": "tibetan mastiff",
        "colour": "black",
        "age": 3,
        "next_checkup": "2020-6-10",
        "vaccinations": ["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
        "sold" : false
    },
    {
        "id": 5,
        "name": "peanut",
        "breed": "beagle",
        "colour": "black/brown",
        "age": 9,
        "next_checkup": "2019-12-10",
        "vaccinations": ["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
        "sold" : false
    },
    {
        "id": 6,
        "name": "tyson",
        "breed": "gaddi",
        "colour": "black",
        "age": 0.4,
        "next_checkup": "2020-1-15",
        "vaccinations": ["Distemper shot", "Rabies"],
        "sold" : false
    }
]

module.exports = {
    result
}

// db.mypets.find().forEach(function(doc) {
//     insert({weight : 18})
//     })

//     db.mypets.insert({
//         id: 4,
//         name: "broto",
//         breed: "lokal indonesia",
//         colour: "rainbow",
//         weight : 22,
//         age: 3,
//         next_checkup: "2020-6-10",
//         vaccinations: ["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
//         "sold" : false
//     }

//     db.mypets.aggregate(
//         [
//         {
//         $group : {
//         _id : $cr_dr ,
//         count : {$sum : 1},
//         totalAmount : {$sum : '$age'},
//         averageAmount : {$avg : { $sum : ['$age', '$id']}}
//         }}
//     ])