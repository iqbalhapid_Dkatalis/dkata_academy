'use strict'

const Bcrypt = require('bcrypt')
const Hapi = require('@hapi/hapi')
const Qs = require('qs')

const Log = require('../../log/logger')
const { connectDB } = require('./connectMongo') 
const userss = require('./usersObject')
const petController = require('../controller/petController')
const route = require('../routes/petRoute')
const CatboxRedis = require('@hapi/catbox-redis');

const server = new Hapi.server({
    port: 3000,
    host: 'localhost',
    query: {
        parser: (query) => Qs.parse(query)
    },
    cache: [
        {
            name: 'my_cache',
            provider: {
                constructor: CatboxRedis,
                options: {
                    partition: 'my_cached_data',
                    host: '127.0.0.1',
                    port: 6379,
                    db: 0
                }
            }
        }
    ]
});

const getPet = petController.getPet

server.method('getPet', getPet, {
    cache : {
        cache: 'my_cache',
        expiresIn: 100 * 1000,
        segment : 'mypets',
        generateTimeout: 2000
    }
});


const validate = async(request, username, password, h) => {
    if (username === "help"){
        return { response : h.redirect('http://hapijs.com/help')}
    } 
    const user = userss.users[username]
    if (!user) {
        return { credentials : null, isValid: false };
    }    
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name }
    return { isValid, credentials }
}

connectDB()
// server.register(require('@hapi/basic'))
// server.auth.strategy('simple', 'basic', { validate });
// server.auth.default('simple');
// server.route(route.routes); //uncomment for server in testing mode

const start = async () => {
    await server.register([
        {
            plugin: require('hapi-pino'),
            options: {
                prettyPrint: process.env.NODE_ENV !== 'production',
                stream: './log/logs.log',
                redact: ['req.headers.authorization']
            }
        },
        require('@hapi/basic')
    ])
    server.auth.strategy('simple', 'basic', { validate });
    server.auth.default('simple');
    server.route(route.routes); //uncomment for running server as onlive

    server.logger().info('another way for accessing it')
    server.log(['subsystem'], 'third way for accessing it')
    await server.start()
    console.log('Server running at:', server.info.uri)
    return server
}


const init = async () => {
    await server.initialize();
    return server;
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})


exports.start = start;
exports.init = init;