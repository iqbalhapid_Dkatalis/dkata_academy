
const petController = require('../controller/petController')
'use strict'
const Joi = require('@hapi/joi')

        const basepath = '/api/v1';
        const routes = [
            {
                method : 'GET',
                path: basepath + '/mypets',
                options: {
                    log: {
                        collect: true
                    },
                    auth: 'simple',
                    validate : {
                        query: {
                            sort : Joi.string().valid('id', 'name', 'colour'), //alowwed sort keywords
                            limit : Joi.number().integer(),
                            offset : Joi.number().integer(),
                            filter : {
                                sold: Joi.boolean()
                            }
                        }
                    }
                },
                handler : petController.getListbyQuery
            },
            {
                method : 'GET',
                path: basepath + '/mypets/{id}',
                options: {
                    cache: {
                        expiresIn: 30 * 1000,
                        privacy: 'private'
                    },
                    auth: 'simple',
                    validate : {
                        params: {
                            id : Joi.number().integer().required()
                        }
                    }
                },
                handler : petController.getListbyID
            },
            {
                method: 'POST',
                path: basepath + '/mypets',
                options: {
                    auth: 'simple',
                    validate: {
                        payload: {
                            name: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            breed: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            colour: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            age: Joi.number().integer().required(),
                            next_checkup: Joi.date(),
                            vaccinations: Joi.array(),
                            sold : Joi.boolean()
                        }
                    }
                },
                handler: petController.createNewPet
            },
            {
                method: 'PUT',
                path: basepath + '/mypets/{id}',
                options: {
                    auth: 'simple',
                    validate: {
                        params: {
                            id: Joi.required(),
                            payload: {
                                next_checkup: Joi.date()
                            }
                        }
                    }
                },
                handler: petController.updatePet
            },
            {
                method: 'DELETE',
                path: basepath + '/mypets/{id}',
                options: {
                    log: {
                        collect: true
                    },
                    auth: 'simple',
                    validate: {
                        params: {
                            id: Joi.number().required(),
                            query: {
                                sold: Joi.string()
                            }
                        }
                    },
                    handler: petController.deletePets
                }
            }]

module.exports = { routes }
