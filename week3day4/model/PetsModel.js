const Mongoose = require('mongoose')

const PetsModel = Mongoose.model("mypet", {
    id: Number,
    name: String,
    breed: String,
    colour: String,
    age: Number,
    next_checkup: Date,
    vaccinations: Array,
    sold : Boolean
})

module.exports = {Mongoose, PetsModel}

