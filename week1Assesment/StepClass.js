let input = [['yellow', 'yellow', 'red'], ['yellow', 'red'], ['red'], ['red'], ['yellow', 'red'], ['yellow','yellow', 'red']]
let target = [['red', 'red', 'red'], ['red','red'], ['red'], ['red'], ['red', 'red'], ['red', 'red', 'red']]

class Transformer {
        _mapStackRed(input) {
                return input.map((input) => input.concat('input'))
        }

        _mapStackYellow(input) {
                return input.map((input) => input.concat('yellow'))
        }

        _mapRejectYellow(input) {
                return input.map((input) => input.filter((input) => input != 'yellow'))
        }

        _mapRejectBlue(input) {
                return input.map((input) => input.filter((input) => input != 'blue'))
        }

        _mapContainRed(input) {
                return input.filter((input) => input.includes('red') ? input : null)
        }

        _mapYellowtoRed(input) {
                return input.map((input) => input.map(input => input == 'yellow' ? 'red' : input))
        }

        _mapYellowtoYellowRed(input) {
                return input.map((input) => input.map(input => input == 'yellow' ? ['yellow', 'red'] : input)).map(input => input.join(',')).map(input => input.split(','))
        }

        _mapBluetoRedBlue(input) {
                return input.map((input) => input.map(input => input == 'blue' ? ['red', 'blue'] : input)).map(input => input.split(','))
        }

        __mapReverse(input) {
                return input.map((input) => input.reverse())
        }
}



module.exports = { input, target , Transformer }