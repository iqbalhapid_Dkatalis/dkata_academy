const { transformer } = require('./StepClass')

//level 0
level0 = [
    ['yellow', 'yellow', 'red'],
    ['yellow', 'red'],
    ['red'],
    ['red'],
    ['yellow', 'red'],
    ['yellow', 'yellow', 'red']
]
le0su1 = [
    ['red', 'red', 'red'],
    ['red', 'red'],
    ['red'],
    ['red'],
    ['red', 'red'],
    ['red', 'red', 'red']
]
le0su2 = [
    ['red'],
    ['red'],
    ['red'],
    ['red'],
    ['red'],
    ['red']
]
le0su3 = [
    ['red', 'yellow'],
    ['red', 'yellow'],
    ['red', 'yellow'],
    ['red', 'yellow'],
    ['red', 'yellow'],
    ['red', 'yellow']
]
le0su4 = [
    ['red', 'yellow', 'red'],
    ['red', 'yellow', 'red'],
    ['red', 'yellow', 'red'],
    ['red', 'yellow', 'red'],
    ['red', 'yellow', 'red'],
    ['red', 'yellow', 'red']
]

//level 1
level1a = [
    ['red', 'red'],
    ['red', 'yellow'],
    ['blue', 'yellow'],
    ['blue', 'blue']
]
le1su1 = [
    ['red', 'red', 'red'],
    ['red', 'yellow', 'red'],
    ['red', 'yellow', 'red'],
    ['red', 'red', 'red']
]
le1su2 = [
    ['red', 'red'],
    ['red', 'red']
]
le1su4 = [
    ['red', 'red'],
    ['red', 'red'],
    ['red', 'red'],
    ['red', 'red']
]
level1b = [
    ['blue', 'blue', 'yellow'],
    ['blue', 'red'],
    ['blue', 'red'],
    ['blue', 'blue', 'yellow']
]
le1su3 = [
    ['red', 'blue', 'blue'],
    ['red', 'blue'],
    ['red', 'blue'],
    ['red', 'blue', 'blue']
]

//level 2
level2a = [
    ['brown'],
    ['orange'],
    ['orange'],
    ['yellow'],
    ['yellow'],
    ['yellow'],
    ['orange'],
    ['orange'],
    ['brown']
]
le2su1 = [
    ['brown'],
    ['orange', 'orange'],
    ['brown', 'brown', 'brown'],
    ['orange', 'orange'],
    ['brown']
]
le2su2 = [
    ['brown', 'brown'],
    ['orange', 'brown', 'orange', 'brown'],
    ['brown', 'brown', 'brown', 'brown', 'brown', 'brown'],
    ['orange', 'brown', 'orange', 'brown'],
    ['brown', 'brown']
]
le2su3 = [
    ['brown', 'brown'],
    ['brown'],
    ['brown', 'brown', 'brown', 'brown'],
    ['brown'],
    ['brown', 'brown']
]
le2su5 = [
    ['yellow'],
    ['yellow'],
    ['yellow', 'yellow', 'yellow', 'yellow'],
    ['yellow'],
    ['yellow']
]
level2b = [
    ['brown'],
    ['orange'],
    ['orange'],
    ['brown']
]
le2su4 = [
    ['orange', 'orange', 'orange', 'orange'],
    ['orange', 'orange'],
    ['orange', 'orange'],
    ['orange', 'orange', 'orange', 'orange']]

console.log('\n', transformer(level1a, le1su4).solver())
test('should be pass', () => {
    expect(transformer(level1a, le1su4).solver()).toEqual(le1su4)
})
