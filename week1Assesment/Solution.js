const {input, target, transform } = require('./StepClass') 


class Solution {    

    selector = () => {
        return parseInt((Math.random() * 8.9) + 0)
    }

    manTrial = (num) => {
        if (num < 4) {
            switch (num) {
                case 1:
                    return 'st'
                case 2:
                    return 'nd'
                case 3:
                    return 'rd'
            }
        }
        else {
            return 'th'
        }
    }

    solver = () => {
        let currentTrial = 1
        let maximumTrial = 5000
        while (JSON.stringify(input) != JSON.stringify(target)) {
            num1 = selector()
            num2 = selector()
            num3 = selector()
            num4 = selector()
            num5 = selector()
            console.log('\n\x1b[33m%s%s Trial\x1b[0m', currentTrial, manTrial(currentTrial))
            console.log('With formula: \x1b[31m%s %s %s %s %s\x1b[0m', num1, num2, num3, num4, num5)
            console.log(num1)
            result = possible[num1].code(input)
            console.log(possible[num1].title + '\n', result)
            if (JSON.stringify(result) == JSON.stringify(target)) {
                input = result
                break
            }
            console.log(num2)
            result = possible[num2].code(result)
            console.log(possible[num2].title + '\n', result)
            if (JSON.stringify(result) == JSON.stringify(target)) {
                input = result
                break
            }
            console.log(num3)
            result = possible[num3].code(result)
            console.log(possible[num3].title + '\n', result)
            if (JSON.stringify(result) == JSON.stringify(target)) {
                input = result
                break
            }
            console.log(num4)
            result = possible[num4].code(result)
            console.log(possible[num4].title + '\n', result)
            if (JSON.stringify(result) == JSON.stringify(target)) {
                input = result
                break
            }
            console.log(num5)
            result = possible[num5].code(result)
            console.log(possible[num5].title + '\n', result)
            if (JSON.stringify(result) == JSON.stringify(target)) {
                input = result
                break
            }
            currentTrial++
            result = input
            if (currentTrial > maximumTrial) {
                console.log('too many try')
                break
            }
        }
        return input
    }

}

module.exports = { Solution }





