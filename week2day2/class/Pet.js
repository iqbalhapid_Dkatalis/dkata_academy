class Pet {
    constructor(type, name, age, gender) {
        this.type = type
        this.name = name
        this.age = age
        this.gender = gender
    }
}

// class  extends Person {
//     constructor(first, last, age, gender, address, interest) {
//         super(first, last, age, gender, address, 'Trainee')
//         this.interest = interest
//     }
// }


module.exports = {
    Pet
}
