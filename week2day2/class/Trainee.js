class Person {
    constructor(first, last, age, gender, address, type) {
        this.first = first
        this.last = last
        this.age = age
        this.gender = gender
        this.address = address
        this.type = type
    }
}

class Trainee extends Person {
    constructor(first, last, age, gender, address, interest) {
        super(first, last, age, gender, address, 'Trainee')
        this.interest = interest
    }
}


module.exports = {
    Trainee: Trainee
}
