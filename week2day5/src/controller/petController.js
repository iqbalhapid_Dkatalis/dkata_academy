const { result } = require('../../class/listMypets')
const mypets = result;

exports.getListbyQuery = (request, h) => {
    let { sort, limit, offset, filter } = request.query //destructuring
    let toShow = () => {
        switch (filter) {
            case 'all':
                return mypets
            case 'sold':
                return mypets.filter(x => x.sold == true)
            default :
                return mypets.filter(x => x.sold != true)
        }
    }
    let availablePet = toShow()
    if (sort, limit, offset != undefined){
        const sortBy = request.query.sort
        const limit = request.query.limit
        const offset = request.query.offset
        let result = mypets.sort((a, b) => (a[sortBy] > b[sortBy]) ? 1 : -1).slice(offset, limit + offset).filter(status => status == false)
        return h.response({ result }).code(200)
    }
    return h.response(availablePet)
}

exports.createNewPet = (request, h) => {
    let currentId = (Math.max(...mypets.map(x => x.id)))
    request.payload.id = ++currentId
    let newPet = mypets.push(request.payload)

    if (newPet) {
        return h.response({ message: 'created' }).code(201)
    }
    return h.response('invalid payload').code(400)
}

exports.updatePet = (request, h) => {
    const idx = mypets.findIndex(x => x.id == request.params.id);
    if (idx != undefined) {
        Object.assign(idx, { next_checkup: request.payload.next_checkup })
        return h.response({ message: 'updated' }).code(202)
    }
    return h.response({ message: 'id not found' }).code(404)
}

exports.deletePets = (request, h) => {
    let availablePet = mypets.filter(x => x.sold != true) 
    if (availablePet.find(x => x.id == request.params.id) != undefined){
        Object.assign(mypets.find(x => x.id == request.params.id), { sold: true})
        return h.response({ message : 'deleted succes', deletedID : request.params.id, remaining: availablePet.length} ).code(202)
    } else {
        return h.response({ message : 'Page not found'}).code(404)
    }
}
