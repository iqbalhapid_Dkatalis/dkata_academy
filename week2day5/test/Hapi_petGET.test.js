'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('..');
const auth = btoa("john:secret")
const { result } = require('../class/listMypets')
const mypets = result;


describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'GET',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: basepath + '/mypets'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'GET',
            headers: {
                Authorization: `Basic ${btoa('admin:admin')}`
            },
            url: basepath + '/mypets'
        });
        expect(res.result.message).to.equal('Bad username or password');
    });


    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'GET',
            url: basepath + '/mypets'
        });
        expect(res.result.message).to.equal('Missing authentication');
    });
})