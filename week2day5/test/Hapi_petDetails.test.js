'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('..');

const { result } = require('../class/listMypets')
const mypets = result;


describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it("the respond returning all my pets", async () => {
        const res = await server.inject({
            method: 'GET',
            url: basepath + '/mypets', 
        })
        let allPet = {result};
        expect(res.result).to.equal(allPet);
    });

    it("the respond returning one single of my pets", async () => {
        const res = await server.inject({
            method: 'GET',
            url: basepath + '/mypets/3'
        })
        let pet = mypets[3]
        expect(res.result).to.equal(pet);
    });

    it("the respond returning one single of my pets", async () => {
        const res = await server.inject({
            method: 'POST',
            url: basepath + '/mypets/list',
            payload : {
                    "name": "jia",
                    "breed": "english mastiff",
                    "colour": "yellow",
                    "age": 6,
                    "next_checkup": "2020-1-2",
                    "vaccinations":["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"]
                    }
        })
        expect(res.result.message).to.equal('created');
    });

    it("the respond returning one single of my pets", async () => {
        const res = await server.inject({
            method: 'DELETE',
            url: basepath + '/mypets/1'
        })
        let pet = mypets.find( x => 1)
        expect(res.result).to.equal(pet);
    });

})