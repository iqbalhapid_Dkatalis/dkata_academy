const userAcc = {
    iqbal : {
        username : 'iqbal',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',
        name : 'John Doe',
        id: '2133d32a'
    }
}

module.exports = {userAcc}