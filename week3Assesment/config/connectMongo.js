const mongoose = require('mongoose')
const mongoUrl = 'mongodb://localhost:27017/dkatalis'

const connectMongo = async () => {
    try {
        await mongoose.connect(mongoUrl, {
            useNewUrlParser : true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        console.log(`MongoDB connect at : ${mongoUrl}`)
        mongoose.set('debug', true)
    }catch{
        process.exit(1)
    }
}

module.exports = {connectMongo}