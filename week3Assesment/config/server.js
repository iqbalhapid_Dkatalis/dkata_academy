'use strict'
const Path = require('path')
const Bcrypt = require('bcrypt')
const Hapi = require('@hapi/hapi')
const Qs = require('qs')
const CatboxRedis = require('@hapi/catbox-redis')

const userAcc = require('./userInfo')
const {connectMongo} = require('./connectMongo')
const todoController = require('../src/controller/todoController')
const route = require('../src/routes/todoRoute')


const server = new Hapi.server({
    port: 3000,
    host: 'localhost',
    routes : {
        files : {
            relativeTo : Path.join(__dirname, '../public')
        }
    }
})


//server method func related


//validation


//connectMongo
connectMongo()



const start = async () => {
    //all config server code write here
    await server.register([
        require('@hapi/inert')
    ])
    server.route(route.routes)
    await server.start()
    console.log('Server running at: ', server.info.uri)
    return server
}


const init = async () => {
    await server.initialize();
    return server;
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})


exports.start = start;
exports.init = init;