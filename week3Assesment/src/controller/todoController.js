const model = require('../model/todoModel')
const result = model.todoModel


//create addtional validator function 

//route endpoint of API
exports.rootPath = async (request, h) => {
    return h.file('index.html')
}

exports.getTodo = async (request, h) => {
    return result.find()
}

exports.getTodobyDay = async (request, h) => {
    let day = request.params.day
    return result.find({day : day})
}

exports.postTodo = async (request, h) => {
    let dueDate = request.payload.created_for
    let currentDay = new Date()

    if(dueDate <= currentDay){
        return h.response({message: 'invalid type on dueDate'}).code(403)
    }                                                                               //checking validation on due date

    let options = { weekday: 'long'};
    let day = new Intl.DateTimeFormat('en-US', options).format(dueDate);            //getting due day at create

    Object.assign(request.payload, {day : day})                       //inserting new one
    let newTodo = await result.insertMany([request.payload])
    h.response().code(201)
    return newTodo
}

exports.updateTodo = async (request, h) => {
    let expectDate = request.params.created_for
    let update = await result.findOneAndUpdate({created_for : expectDate}, request.payload)
    return update
}

exports.deleteTodo = async (request, h) => {
    let expectDate = request.params.created_for
    let deleted = await result.findOneAndUpdate({ created_for: expectDate }, {status : 'completed'})
    return deleted;
}