'use strict'
const todoController = require('../controller/todoController')
const Joi = require('@hapi/joi')

const basepath = '/api/v1'
const routes = [
    {
        method: 'GET',
        path: '/',
        handler: todoController.rootPath
    },
    {
        method: 'GET',
        path: basepath + '/todos',
        handler: todoController.getTodo
    },
    {
        method: 'GET',
        path: basepath + '/todos/{day}',
        options : {
            validate : {
                params : {
                        day : Joi.string().valid('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday').required()
                }
            }
        },
        handler: todoController.getTodobyDay
    },
    {
        method: 'POST',
        path: basepath + '/todos',
        options : {
            validate : {
                payload : {
                    title : Joi.any().required(),
                    body : Joi.any().required(),
                    created_for : Joi.date()
                }
            }
        },
        handler: todoController.postTodo
    },
    {
        method: 'PUT',
        path: basepath + '/todos/{created_for}',
        options : {
            validate : {
                params : {
                        created_for : Joi.date().required()
                },
                payload : {
                    title : Joi.any().required(),
                    body : Joi.any().required(),
                    status : Joi.string().required(),
                    comments: Joi.array()
                }
            }
        },
        handler: todoController.updateTodo
    },
    {
        method: 'DELETE',
        path: basepath + '/todos/{created_for}',
        options : {
            validate : {
                params : {
                        created_for : Joi.date().required()
                }
            }
        },
        handler: todoController.deleteTodo
    }
]


module.exports = {routes}