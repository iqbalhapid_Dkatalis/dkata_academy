const Mongooose = require('mongoose')
const { Schema } = Mongooose

const todoSchema = new Schema({
    title : String,
    body : String,
    comments : [{body: String, date: {type : Date, default: Date.now}}],
    created_at : {type : Date, default: Date.now},
    updated_at : {type : Date, default: null},
    created_for : {type : Date},
    day : String,
    status : {type : String, default: 'new'}
})

const todoModel = Mongooose.model('task', todoSchema);

module.exports = { todoModel }