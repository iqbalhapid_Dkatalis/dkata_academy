
const petController = require('../controller/petController')
'use strict'
const Joi = require('@hapi/joi')

        const basepath = '/api/v1';
        const routes = [
            {
                method : 'GET',
                path: basepath + '/mypets',
                options: {
                    auth: 'simple',
                    validate : {
                        query: {
                            sort : Joi.string().valid('type', 'name', 'age', 'gender'), //alowwed sort keywords
                            limit : Joi.number().integer().min(1),
                            offset : Joi.number().integer().min(1),
                            status : Joi.string()
                        }
                    }
                },
                handler : petController.getListbyQuery
            },
            {
                method: 'POST',
                path: basepath + '/mypets',
                    options: {
                        auth: 'simple',
                    validate: {
                        payload: {
                            name: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(), //alowwed sort keywords
                            breed: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            colour: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            age: Joi.number().integer().required(),
                            next_checkup: Joi.date(),
                            vaccinations: Joi.array()
                        }
                    }
            },
                handler: petController.createNewPet
            },
            {
                method: 'PUT',
                path: basepath + '/mypets/{id}',
                options: {
                    auth: 'simple',
                    validate: {
                        params: {
                            id: Joi.required(),
                            payload: {
                                next_checkup: Joi.date()
                            }
                        }
                    }
                },
                handler: petController.updatePet
            },
            {
                method: 'DELETE',
                path: basepath + '/mypets/{id}',
                options: {
                    auth: 'simple',
                    validate: {
                        params: {
                            id: Joi.number().required(),
                            query: {
                                name: Joi.string()
                            }
                        }
                    },
                    handler: petController.deletePets
                }
            }]

module.exports = { routes }
