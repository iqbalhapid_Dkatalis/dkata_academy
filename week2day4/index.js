'use strict'

const Bcrypt = require('bcrypt')
const Hapi = require('@hapi/hapi')
const Log = require('./log/logger')

const userss = require('./src/config/usersObject')
const route = require('./src/routes/petRoute')

const validate = async(request, username, password) => {
    const user = userss.users[username]
    if (!user) {
        return { credentials : null, isValid: false };
    }
    
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name }
    // console.log("credentials", credentials);
    return { isValid, credentials }
}

const server = new Hapi.server({
    port: 3000,
    host: 'localhost'
});

const init = async () => {
    try {
        await server.register(require('@hapi/basic')) 
        server.auth.strategy('simple', 'basic', { validate });
        server.route( route.routes);
        await server.start()
    } catch (e) {
        console.log(e)
        process.exit(1)
    }
    console.log('Server running at:', server.info.uri)
    server.events.on('response', (request) => {
        Log(request);
    });
}

exports.init = async () => {
    await server.initialize();
    return server;
}

exports.start = async () => {
    await server.start();
    return server;
}

init();