const axios = require('axios');
const fs = require('fs');
const path = require("path");

const url = "https://xkcd.com/info.0.json";

const getData = async (url) => {
    try {
        const response = await axios.get(url);
        const data = response.data;
        let uri = data.img;
        let name = path.basename(uri);
        let file = fs.createWriteStream(`./${name}.jpg`);
        data.pipe(file) //pipe the image url to Writestream
    } catch (error) {
        return error
    }
}

getData(url);
