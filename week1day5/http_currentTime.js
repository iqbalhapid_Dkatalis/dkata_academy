const http = require('http');
const url = require('url');

let server = http.createServer(function (request, response) {
    if (request.method === 'GET') {
        let parsedURL = url.parse(request.url, true);//parsing url into objectString 
        let path = parsedURL.pathname;
        var date = new Date(parsedURL.query.iso);    //format in ISO format
        if (path === '/api/parsetime') {             //url Path (endpoint)
            var output = { //storing output as Object literal
                hour: date.getUTCHours(),
                minute: date.getMinutes(),
                second: date.getSeconds()
            };
        }
        if (output) {
            response.writeHead(200, { 'content-type': 'application/json' });
            response.end(JSON.stringify(output)); //A common use of JSON is to exchange data to/from a web server. 
                                                  //When sending data to a web server, the data has to be a string.
                                                  //Convert a JavaScript object into a string with JSON.stringify().
        }else{
            response.end('the URL link is invalid') //error handler for invalid type or format
        }
    }
});
server.listen(9000);



// var http = require('http');
// var url = require('url');

// http.createServer(function (req, res) {
//     res.writeHead(200, { 'Content-Type': 'text/html' });
//     var q = url.parse(req.url, true).query;
//     var txt = q.year + " " + q.month;
//     res.end(txt);
// }).listen(8080);