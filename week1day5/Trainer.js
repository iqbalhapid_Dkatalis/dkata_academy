class person {
    constructor(first, last, age, gender, address, type) {
        this.first = first
        this.last = last
        this.age = age
        this.gender = gender
        this.address = address
        this.type = type
    }
}

class trainee extends person {
    constructor(first, last, age, gender, address, interest) {
        super(first, last, age, gender, address, 'trainee')
        this.interest = interest
    }
}


module.exports = {
    trainee: trainee
}
