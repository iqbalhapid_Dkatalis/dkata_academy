const https = require('https');

https.get('https://xkcd.com/info.0.json', (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
        data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
        console.log(JSON.parse(data).month);
        console.log(JSON.parse(data).year);
        console.log(JSON.parse(data).safe_title);
        console.log(JSON.parse(data).alt);
    });

}).on("error", (err) => {
    console.log("Error: " + err.message);
});