'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('..');

const { result } = require('../class/listMypets')
const mypets = result;


describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it('Trying to delete some pet', async () => {
        const res = await server.inject({
            method: 'DELETE',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: basepath + '/mypets/2'
        })
        const soldedpet = await server.inject({
            method: 'GET',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: basepath + '/mypets?filter=sold'
        })
        expect(res.result.message).to.equal('deleted success')
        expect((soldedpet.result).length).to.equal(1)
    })
});