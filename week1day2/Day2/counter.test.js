const counter = require('./counter');

const myCounter1 = counter();

const myValue1 = myCounter1.getCurrentValue();
test('current value on myCounter1 object', () => {
    expect(myValue1).toBe(0);
})

const myIncrement1 = myCounter1.increment();
test('incrementing currentvalue plus one', () => {
    expect(myIncrement1).toBe(1);
})

const myDecrement1 = myCounter1.decrement();
test('decrementing currentValue minus one', () => {
    expect(myDecrement1).toBe(0);
})