const counter = function () {
    let currentValue = 0;
    const increment = function () {
        currentValue += 1;
        return currentValue;
    }
    const decrement = function () {
        currentValue -= 1;
        return currentValue;
    }
    
    const getCurrentValue = function () {
        return currentValue;
    }

    return { increment, decrement, getCurrentValue }
}

module.exports = counter;

// let myCounter = counter()
// console.log(myCounter.getCurrentValue())
// myCounter.increment()
// myCounter.increment()
// console.log(myCounter.getCurrentValue())
// myCounter.decrement()
// console.log(myCounter.getCurrentValue())

// let anotherCounter = counter()
// console.log(anotherCounter.getCurrentValue())
// console.log(myCounter.getCurrentValue())
// console.log(anotherCounter.currentValue) //undefined


