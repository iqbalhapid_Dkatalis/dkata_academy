sum = (total, num) => {
    return total + num;         //destructuting
}

myReduce = (city) => { 
    return city.map( c => c.length ).filter( c => c > 6).reduce(sum);
} 

module.exports = myReduce;