var MymathFunction = require('./exponent')

//testing function
const myExpo = MymathFunction.expo(2,2);
test('2 power 2 equal to be 4', () => {
    expect(myExpo).toBe(4)
})

const mySquare = MymathFunction.sqr(3);
test('2 with reuse expo function equal to 4', () => {
    expect(mySquare).toEqual(9)
})

const myCube = MymathFunction.cube(2);
test('2 power 3, reuse expo function equal to 8', () => {
    expect(myCube).toBe(8)
})

const newExp=MymathFunction.expo(3,4)
test('3 exponents 4 equal to 81',()=>{
    expect(newExp).toBe(81)
})