const {start} = require('./src/config/server')

start().catch((err) => {
    console.log(err)
    process.exit(1)
})


