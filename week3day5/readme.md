### Features

- an API PetList using Hapijs and MongoDB with mongoose
- Rest API
- Full-featured: Validation, Authentification, Caching, Logging, Testable

### How to Run
`$ npm install`
`$ node start`
`open your browser and go to localhost:3000/api/v1/mypets`
`Authorization using username : john  password: secret`


###The problems during development

> the validation still using basic Hapi, need more improvement
> user data for authorization still an object not as data in database
> test files need more cases


###Most challenging during development
                    
> applying cache management in redis as inteface for stroring cache。
> managing the respond time on server for passed in some test cases
> chaining mongoodb query  but dont reduce the server perfomance
> hard to find optimize way when doing some query on mongoodb.