
const petController = require('../controller/petController')
'use strict'
const Joi = require('@hapi/joi')

        const basepath = '/api/v1';
        const routes = [
            {
                method : 'GET',
                path: basepath + '/mypets',
                options: {
                    description: 'Get list of pet',
                    notes: 'Returns an array of object pets',
                    tags: ['api/v1/mypets'],
                    log: {
                        collect: true
                    },
                    auth: 'simple',
                    validate : {
                        query: {
                            sort : Joi.string().valid('id', 'name', 'colour'), //alowwed sort keywords
                            limit : Joi.number().integer(),
                            offset : Joi.number().integer(),
                            filter : {
                                sold: Joi.boolean()
                            }
                        }
                    }
                },
                handler : petController.getListbyQuery
            },
            {
                method : 'GET',
                path: basepath + '/mypets/{id}',
                options: {
                    description: 'Get pet by id',
                    notes: 'Returns an object pets by corresponding id',
                    tags: ['api/v1/mypets/id'],
                    cache: {
                        expiresIn: 30 * 1000,
                        privacy: 'private'
                    },
                    auth: 'simple',
                    validate : {
                        params: {
                            id : Joi.number().integer().required()
                        }
                    }
                },
                handler : petController.getListbyID
            },
            {
                method: 'POST',
                path: basepath + '/mypets',
                options: {
                    description: 'posting your new beloved pet',
                    notes: 'Returns an status message (created)',
                    tags: ['api/v1/mypets'],
                    auth: 'simple',
                    validate: {
                        payload: {
                            name: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            breed: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            colour: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                            age: Joi.number().integer().required(),
                            next_checkup: Joi.date(),
                            vaccinations: Joi.array(),
                            sold : Joi.boolean()
                        }
                    }
                },
                handler: petController.createNewPet
            },
            {
                method: 'PUT',
                path: basepath + '/mypets/{id}',
                options: {
                    description: 'Update one of pet by corresponding ID',
                    notes: 'Returns a message status (updated)',
                    tags: ['api/v1/mypets/id'],
                    auth: 'simple',
                    validate: {
                        params: {
                            id: Joi.required(),
                            payload: {
                                next_checkup: Joi.date()
                            }
                        }
                    }
                },
                handler: petController.updatePet
            },
            {
                method: 'DELETE',
                path: basepath + '/mypets/{id}',
                options: {
                    description: 'Deleting one pet by corresponding Id',
                    notes: 'returning a message status (',
                    tags: ['api/v1/mypets'],
                    log: {
                        collect: true
                    },
                    auth: 'simple',
                    validate: {
                        params: {
                            id: Joi.number().required(),
                            query: {
                                sold: Joi.string()
                            }
                        }
                    },
                    handler: petController.deletePets
                }
            }]

module.exports = { routes }
