const model = require('../../model/PetsModel')
const result = model.PetsModel;


const isAvailable = async (id) => {
    let apiReq = await result.find({id: id}, {_id :0 })
    let notAvaliable = !apiReq[0] ? true : false
    return notAvaliable
}

exports.getListbyQuery = async (request, h) => {
    let query = request.query
    let sort = query.sort ? query.sort : 'id'
    let limit = query.limit ? query.limit : 10
    let offset = query.offset ? query.offset : 0
    let filter = request.query.filter
    
    return result.find(filter, {_id: 0 , sold: 0})
    .sort(sort)
    .limit(limit)
    .skip(offset)
}

exports.getPet = async (getId) => {
    return await result.findOne({id : getId}).lean()
}

exports.getListbyID = (request, h) => {
    let getId = request.params.id
    return request.server.methods.getPet(getId)
    //return result.find({"id":getId})
}


exports.createNewPet = async (request, h) => {
    maxId = await result.aggregate([{$group:{_id:"maxId", max:{$max: "$id"}}}])
    newId = maxId[0].max + 1
    Object.assign(request.payload, {id : newId, sold : false})
    newPet = await result.insertMany([request.payload])
    h.response().code(201)
    return newPet
}

exports.updatePet = async (request, h) => {
    const next_checkup =  request.payload.next_checkup;
    const id = request.params.id
    if (await isAvailable(id)){
        return h.response({message : 'id not found'}).code(404)    
    }
    await result.findOneAndUpdate({id, next_checkup});
    await request.server.methods.getPet.cache.drop(id)
    h.response().code(202)
    return request.server.methods.getPet(id)
}    

exports.deletePets = async (request, h) => {
    let id = request.params.id
    if (await isAvailable(id)){
        return h.response({message : 'id not found'}).code(404)    
    }
    let availablePet = await result.find({id: id, sold : false})
    if (!availablePet[0]) {
        return h.response({ message: 'pet has sold' }).code(404)
    } else {
        await result.findOneAndUpdate({ id: id }, { sold: true })
        await request.server.methods.getPet.cache.drop(id)
        h.response().code(202)
        return request.server.methods.getPet(id)
    }
}
