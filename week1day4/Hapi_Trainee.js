'use strict'

const Hapi = require('@hapi/hapi')
const { Trainee } = require('./class/Trainee')

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route([
        {
            method: 'GET',
            path: '/',
            handler: (request, h) => {
                return 'Hello World!';
            }
        },
        {
            method: 'GET',
            path:'/radha',
            handler: (request, h) => {
                return 'Hai Radha'
            }
        },
        {
            method: 'GET',
            path:'/trainee',
            handler: (request, h) => {
                let iqbal = new Trainee('iqbal', 'hapid', 22, 'male', 'bandung', 'JavaScript')
                // const traineeJson = JSON.stringify(iqbal);
                return iqbal;
            }
        }
    ]);

    await server.start();
    console.log('Server running on %s ', server.info.uri);
}

    process.on('unhandledRejection', (err) => {
        
        console.log(err);
        process.exit(1);
    })

init();