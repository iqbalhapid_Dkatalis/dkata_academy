'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../Hapi_Allcases');

describe('GET /', () => {
    let server;

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it("respond with 200", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/'
        })
        expect(res.statusCode).to.equal(200);
    });

    it("respond will return string", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/radha'
        })
        expect(res.payload).to.equal('Hai Radha');
    });

    it("respond will return JSON", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/trainee'
        })
        const output = {"first":"iqbal","last":"hapid","age":22,"gender":"male","address":"bandung","type":"Trainee","interest":"JavaScript"};
        expect(res.result).to.equal(output);
    });
    

    it("respond will return JSON", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/api/parsetime?iso=2013-08-10T12:10:15.474z'
        })
        const output = {"hour":12, "minute":10, "second": 15};
        expect(res.result).to.equal(output);
    });

    
    it("respond will return parameter", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/iqbal'
        })
        expect(res.request.params.name).to.equal('iqbal');
    });

});