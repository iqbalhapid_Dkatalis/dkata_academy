'use strict'

const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi')
const { Trainee } = require('./class/Trainee')

// const init = () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route([
        {
            method: 'GET',
            path: '/',
            handler: (request, h) => {
                return 'Hello World!';
            }
        },
        {
            method: 'GET',
            path:'/radha',
            handler: (request, h) => {
                return 'Hai Radha'
            }
        },
        // { //some new improvement at bottom file with adding validation 
        //     method: 'GET',
        //     path: '/{name}',
        //     handler: (request, h) => {
        //         return `hai ${request.params.name}`
        //     }
        // },
        {
            method: 'GET',
            path:'/trainee',
            handler: (request, h) => {
                let iqbal = new Trainee('iqbal', 'hapid', 22, 'male', 'bandung', 'JavaScript')
                // const traineeJson = JSON.stringify(iqbal);
                return iqbal;
            }
        },
        {
            method: 'GET',
            path:'/api/parsetime',
            handler: (request, h) => {
                const date = new Date(request.query.iso); 
                var output = { //storing output as Object literal
                    hour: date.getUTCHours(),
                    minute: date.getMinutes(),
                    second: date.getSeconds()
                };
            return output;
            }
        },
        {
                method: 'GET',
                path: '/{name}',
                options: {
                    validate: {
                        params: {
                            name: Joi.string().regex(/^[A-Za-z]+$/).min(3)
                        }
                    }       
                },
            handler: (request, h) => {
                return `hai ${request.params.name}`
            }
        }
    ]);

    // await server.start();
    // console.log('Server running on %s ', server.info.uri);
// }

    process.on('unhandledRejection', (err) => {
        
        console.log(err);
        process.exit(1);
    })

exports.init = async () => {
    await server.initialize();
    return server;
}

exports.start = async () => {
    await server.start();
    return server;
}

// init();