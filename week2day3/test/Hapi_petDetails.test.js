'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../Hapi_petDetails');

const { result } = require('../class/listMypets')
const mypets = result;


describe('GET /', () => {
    let server;

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it("the respond returning all my pets", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/mypets'
        })
        let allPet = mypets;
        expect(res.result).to.equal(allPet);
    });

    it("the respond returning one single of my pets", async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/mypets/3'
        })
        let pet = mypets[3]
        expect(res.result).to.equal(pet);
    });

    // it("the respond returning one single of my pets", async () => {
    //     const res = await server.inject({
    //         method: 'GET',
    //         url: '/mypets/api'
    //     })
    //     let pet = mypets[3]
    //     expect(res.result).to.equal(pet);
    // });

})