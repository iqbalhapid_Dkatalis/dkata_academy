'use strict'

const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi')

const { result } = require('./class/listMypets')
const mypets = result;

const init = async () => { //for testing purpose i need to comment init() 

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route([
        {
            method:'GET',
            path: '/mypets',
            handler : (request, h) => {

                return mypets;
            }
        },
        {
            method: 'GET',
            path: '/mypets/{id}',
            options: {
                validate : {
                    params: {
                        id: Joi.number().integer().required().min(1)
                    },
                    query: {
                        page: Joi.number().min(0).default(1),
                    }
                }
            },
            handler: (request, h) => {
            

                return mypets[+request.params.id];
            }
        },
        {
            method: 'GET',
            path: '/mypets/api',
            options: {
                validate : {
                    query: {
                        sort : Joi.string().valid('type', 'name', 'age', 'gender'), //alowwed sort keywords
                        limit : Joi.number().integer().min(1),
                        offset : Joi.number().integer().min(1)
                    }
                }
            },
                handler: function (request, h) {
                    const sortBy = request.query.sort
                    const limit = request.query.limit
                    const offset = request.query.offset
                    let result = mypets.sort((a, b) => (a[sortBy] > b[sortBy]) ? 1 : -1).slice(offset, limit + offset)
                    return result;
            }
        },
        {
            method: 'POST',
            path: '/mypets/list',
            options: {
                validate: {
                    payload: {
                        name: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(), //alowwed sort keywords
                        breed: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                        colour: Joi.string().regex(/^[A-Za-z\s]{3,}$/).required(),
                        age: Joi.number().integer().required(),
                        next_checkup : Joi.date(),
                        vaccinations : Joi.array()
                    }
            }
            },
            handler: function (request, h) {
                    let curId = (Math.max(...mypets.map(x => x.id)))
                    request.payload.id = ++curId
                    let newPet = mypets.push(request.payload)
                    
                    if (newPet){
                        return h.response({ message: 'created' }).code(201)
                    }
                    return h.response('').code(400)
                }
        },
        {
            method: 'PUT',
            path: '/mypets/list/{id}',
            options: {
                validate: {
                    params : {
                            id : Joi.required(),
                    payload: {
                        next_checkup : Joi.date()
                    }
                }
            }
            },
            handler: function (request, h) {
                if(mypets.find(x => x.id == request.params.id) != undefined){
                    Object.assign(mypets.find(x => x.id == request.params.id), { next_checkup : request.payload.next_checkup })
                    return h.response({message: 'updated'}).code(202)
                }
                    return h.response({message : 'not found'}).code(404)
                }
        }
    ]);

    await server.start();
    console.log('Server running on %s ', server.info.uri);
}

    process.on('unhandledRejection', (err) => {
        
        console.log(err);
        process.exit(1);
    })

    exports.init = async () => {
        await server.initialize();
        return server;
    }
    
    exports.start = async () => {
        await server.start();
        return server;
    }

init();