const glob = require('glob');
// print process.argv
const finder = () => {
    let path = process.argv[2]

    const findImg = glob.sync(path, {}) //using synchronus to avoid "undefined" because async process
    
    
    return findImg;
}

module.exports = { finder };


//hardcoded method but still dont know use files filtering
// const path = require('path')
// const fs = require('fs')

// const directoryPath = path.join(__dirname, '../Day4/img');
// fs.readdir(directoryPath, function (err, files) {
//     if (err) {
//         return 'Unable to scan directory: ' + err;
//     } 
//     //listing all files using forEach
//     files.forEach(function (file) {
//         // Do whatever you want to do with the file
//         console.log(file); 
//     });
// });
