const ImageSorter = require('./ImageSorter')
const receive = [
    '../Day4/img/20160816_200354.jpg',
    '../Day4/img/40378.jpg',
    '../Day4/img/432291_184359875010308_100003088896562_296062_1231020730_n_400x400.jpg',
    '../Day4/img/44008.jpg',
    '../Day4/img/736817_sony-vaio-wallpapers-for-desktop-1920x1080-full-hd_1920x1080_h.jpg',
    '../Day4/img/great-presentation-group-of-business-people-in-smart-casual-wear-sitting-together-addressing-applauding-after-speech-of-a-businessman-at-the-conference-meeting_1423-326.jpg',
    '../Day4/img/honda-.jpg',
    '../Day4/img/idea.jpg',
    '../Day4/img/jonatan-pie-234237_Mac.jpg',
    '../Day4/img/Memiliki-Inisiatif-Tinggi.jpg',
    '../Day4/img/sample.jpg'
];

const sorter = ImageSorter.finder();
test('the return value is array of filenames to be', () => {
    expect(sorter).toEqual(receive)
})


// test('match with equality of an object', () => {
//     const obj = {status : true}
//     expect(obj).toBe({status : true})
// })