const {Transform}  = require('stream')
const fs = require('fs')

String.prototype.capitalize = function(){
    return this.split('\. ').map(x => x.charAt(0).toUpperCase() + x.slice(1)).join('\. ') + '\n'
}

const capitalize = new Transform({
    transform(chunk, encoding, callback){
        this.push(chunk.toString().capitalize())
        callback()
    }
})

const rs = fs.createReadStream('./testWrite/hello.txt')
const ws = fs.createWriteStream('./testWrite/hello_fix.txt')
rs.pipe(capitalize).pipe(ws)


// rl.on('line', (line) => {
//     ws.write(line.capitalize())
// })



// const fs = require('fs')
// const path = require('path')
// const readline = require('readline');

// var myInterface = readline.createInterface({
//     input: fs.createReadStream('./testWrite/hello.txt')
// });

// var lineno = 0;
// myInterface.on('line', function (line) {
//     lineno++;
//     console.log('Line number ' + lineno + ': ' + line);  //reading stream
// });


//     fs.writeFile(path.join(__dirname, '/testWrite', 'hello.txt'), 'i love nodeJs. this is fun. and love this', err => {
//             //callback function
//             if (err) throw err;
//             console.log('file written to...');
//             fs.appendFile(path.join(__dirname, '/testWrite', 'hello.txt'), ' so much', err => {
//                 if (err) throw err;
//                 console.log('file written to...');
//             }); //write and adding more the content if re-run again
//     }) //writing into file