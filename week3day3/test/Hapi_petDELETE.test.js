const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../src/config/server');
const btoa = require('btoa')
const auth = btoa("john:secret")



describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it('Trying to delete some pet', async () => {
        const res = await server.inject({
            method: 'DELETE',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: basepath + '/mypets/2'
        })
        expect(res.result[0].sold).to.equal('true')
    })
});