const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../src/config/server');
const btoa = require('btoa')
const auth = btoa("john:secret")



describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it("the respond returning one single of my pets", async () => {
        const res = await server.inject({
            method: 'PUT',
            url: basepath + '/mypets/6',
            headers: {
                Authorization: `Basic ${auth}`
            },
            payload : {
                    "next_checkup": "9999-1-2",
                    }
        })
        expect(res.result[0].next_checkup).to.equal("9999-1-2");
    });

})