const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../src/config/server');
const btoa = require('btoa')
const auth = btoa("john:secret")



describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it('Try open Pets API', async () => {
        const res = await server.inject({
            method: 'GET',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: basepath + '/mypets'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('Try get api without bad username or password', async () => {
        const res = await server.inject({
            method: 'GET',
            headers: {
                Authorization: `Basic ${btoa('admin:admin')}`
            },
            url: basepath + '/mypets'
        });
        expect(res.result.message).to.equal('Bad username or password');
    });


    it('Try get API with missing auth', async () => {
        const res = await server.inject({
            method: 'GET',
            url: basepath + '/mypets'
        });
        expect(res.result.message).to.equal('Missing authentication');
    });

    it('Try Getting pet by id', async () => {
        const res = await server.inject({
            method: 'GET',
            headers: {
                Authorization: `Basic ${auth}`
            },
            url: basepath + '/mypets/3'
        });
        expect(res.result[0].name).to.equal('xena');
    });
})