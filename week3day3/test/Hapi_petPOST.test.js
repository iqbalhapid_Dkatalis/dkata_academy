const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../src/config/server');
const btoa = require('btoa')
const auth = btoa("john:secret")



describe('GET /', () => {
    let server;
    const basepath = '/api/v1';

    beforeEach( async () => {
        server = await init();
    });

    afterEach( async () => {
        await server.stop();
    });

    it("the respond returning one single of my pets", async () => {
        const res = await server.inject({
            method: 'POST',
            url: basepath + '/mypets',
            headers: {
                Authorization: `Basic ${auth}`
            },
            payload : {
                    "name": "bad dog",
                    "breed": "english mastiff",
                    "colour": "yellow",
                    "age": 6,
                    "next_checkup": "2020-1-2",
                    "vaccinations":["Distemper shot", "Kennel Cough", "Dog Flu", "Rabies"],
                    "sold" : false
                    }
        })
        expect(res.result[0].name).to.equal("bad dog");
    });

})