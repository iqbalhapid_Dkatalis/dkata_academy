const model = require('../../model/PetsModel')
const result = model.PetsModel;


exports.getListbyQuery = async (request, h) => {
    let query = request.query
    let sort = query.sort ? query.sort : 'id'
    let limit = query.limit ? query.limit : 10
    let offset = query.offset ? query.offset : 0
    let filter = request.query.filter
    
    return result.find(filter, {_id: 0 , sold: 0})
    .sort(sort)
    .limit(limit)
    .skip(offset)
}

exports.getListbyID = (request, h) => {
    let getId = request.params.id
    return result.find({"id":getId})
}


exports.createNewPet = async (request, h) => {
    maxId = await result.aggregate([{$group:{_id:"maxId", max:{$max: "$id"}}}])
    newId = maxId[0].max + 1
    Object.assign(request.payload, {id : newId, sold : false})
    newPet = await result.insertMany([request.payload])
    h.response().code(201)
    return newPet
}

exports.updatePet = async (request, h) => {
    const next_checkup =  request.payload.next_checkup;
    const id = request.params.id
    const petUpdated = await result.findOneAndUpdate({id, next_checkup});
    h.response().code(202)
    return petUpdated
}    

exports.deletePets = (request, h) => {
    let availablePet = result.find({sold : false})
    if (availablePet.find({id : request.params.id}) != undefined) {
        const deletedPet = result.update({ id: request.params.id }, { $set: { sold: true } })
        h.response().code(202)
        return deletedPet        
    } else {
        return h.response({ message: 'id not found' }).code(404)
    }
}
