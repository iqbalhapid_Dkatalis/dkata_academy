const mongoose = require('mongoose')
const mongoUrl = 'mongodb://localhost:27017/dkatalis'

const connectDB = async () => {
    try{
        await mongoose.connect(mongoUrl, {
            useNewUrlParser : true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });
        console.log(`MongoDB connect at: ${mongoUrl}`)
    }catch{
        //console.error(err.message)
        process.exit(1)
    }
}

module.exports = {connectDB}