'use strict'

const Bcrypt = require('bcrypt')
const Hapi = require('@hapi/hapi')
const Qs = require('qs')

const Log = require('../../log/logger')
const { connectDB } = require('./connectMongo') 
const userss = require('./usersObject')
const route = require('../routes/petRoute')

const server = new Hapi.server({
    port: 3000,
    host: 'localhost',
    query : {
        parser : (query) => Qs.parse(query)
    }
});

const validate = async(request, username, password, h) => {
    if (username === "help"){
        return { response : h.redirect('http://hapijs.com/help')}
    } 

    const user = userss.users[username]
    if (!user) {
        return { credentials : null, isValid: false };
    }    
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name }
    return { isValid, credentials }
}

connectDB()
// server.register(require('@hapi/basic'))
// server.auth.strategy('simple', 'basic', { validate });
// server.auth.default('simple');
// server.route(route.routes); //uncomment for server in testing mode

const start = async () => {
    await server.register(require('@hapi/basic'))
    server.auth.strategy('simple', 'basic', { validate });
    server.auth.default('simple');
    server.route(route.routes); //uncomment for running server as onlive
    await server.start()
    console.log('Server running at:', server.info.uri)
    server.events.on('response', (request) => {
        Log(request);
    });
    return server
}

const init = async () => {
    await server.initialize();
    return server;
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})


exports.start = start;
exports.init = init;
