'use strict'

const Hapi = require('@hapi/hapi')
const Log = require('./log/logger')


const server = new Hapi.server({
    port: 3000,
    host: 'localhost'
});

const init = async () => {
    try {
        await server.register(require('./src/routes/chatRoute'))
        await server.start()
    } catch (e) {
        console.log(e)
        process.exit(1)
    }
    console.log('Server running at:', server.info.uri)
    server.events.on('response', (request) => {
        Log(request);
    });
    
}

init();