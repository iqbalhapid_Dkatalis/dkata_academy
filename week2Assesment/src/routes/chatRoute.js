const fileController = require('../controller/fileController')

'use strict'

exports.plugin = {
    pkg : require('../../package.json'),
    name : 'route-chat',
    register : async (server, options, next) => {
        server.route([
            {
                method : 'GET',
                path: '/',
                handler : fileController.filePaths
            }   
        ])
    }
}
